export const zoneToClass: { [key: string]: string } = {
  "Green": "green-zone-text",
  "Red": "danger-zone-text",
  "Orange": "orange-zone-text"
}

export const fakeEmployees = [
  {name: "Deepanshu Jain", code: 'de.jain', address: "110045", department: "prod"},
  {name: "Akshay Gupta", code: 'ak.gupta', address: "110037", department: "prod"},
  {name: "Karan Arora", code: 'ka.arora', address: "101213", department: "prod"},
  {name: "Lalit Kumar", code: 'la.kumar', address: "125055", department: "prod"},
  {name: "Kartikeya Mudgal", code: 'la.mudgal', address: "122009", department: "prod"},
]