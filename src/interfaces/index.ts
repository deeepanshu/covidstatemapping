export interface IZone {
  district: string;
  districtcode: string;
  lastupdated: string;
  source: string;
  state: string;
  zone: string;
}