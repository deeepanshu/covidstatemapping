import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Header from './components/Header';
import UploadFile from './components/UploadFile';
import ZoneGrid from './components/ZoneGrid';
import EmployeeGrid from './components/EmployeeGrid';
import Loader from './components/Loader';
import { fakeEmployees } from './constants';
import axios from 'axios';

import * as types from './redux/types';

const API_URL = 'https://api.postalpincode.in/pincode/';

const axiosWrapper = async (pincode: string) => new Promise((resolve, reject) => {
  axios.get(`${API_URL}${pincode}`)
    .then(response => resolve(response.data))
    .catch(error => reject(error.response.data));
})




function App() {

  const [employees, setEmployees] = useState<any[]>(fakeEmployees);
  const [pincodeDistictMap, setPinCodeDistrictMap] = useState<any>({});

  const dispatch = useDispatch();
  const { data: { zones }, app: { loader } } = useSelector(({ data, app }: any) => ({ data, app }));
  console.log(zones);
  
  useEffect(() => {
    const loadData = () => dispatch({ type: types.GET_ZONAL_DATA });
    loadData();
  }, [])

    useEffect(() => { 
      let addresses = employees.map((ele) => ele.address);
      addresses.forEach(ele => {
        console.log(ele);
        if(pincodeDistictMap[ele]) {
          console.log("mila kuch");
          let district = pincodeDistictMap[ele];
          
        }
      })
    }, [pincodeDistictMap])

  const onUploadHandler = async () => {
    // console.log(fakeEmployees);
    let allEmployees = [...employees];
    let allEmpPincodes = allEmployees.map((ele) => ele.address);
    // console.log(allEmpPincodes);
    let distinctPincodes = Array.from(new Set(allEmpPincodes));
    // console.log(distinctPincodes, distinctPincodes.length);

    let i, j, chunkSize = 2;

    for (i = 0, j = distinctPincodes.length; i < j; i += chunkSize) {
      let chunk = distinctPincodes.slice(i, i + chunkSize);
      // console.log(chunk);
      let promises: Promise<unknown>[] = [];
      chunk.forEach(ele => promises.push(axiosWrapper(ele)))

      let resolves = await Promise.all(promises);
      // console.log(resolves);
      for (let k = 0; k < resolves.length; k++) {
        let mapping: any = resolves[k];
        console.log("Mapping", mapping[0])
        if (mapping[0].Status === 'Success') {

          if (Array.isArray(mapping[0].PostOffice)) {
            let pincode = mapping[0].PostOffice[0].Pincode;

            let allDistricts = mapping[0].PostOffice.map((ele: any) => ele.District);
            let distinctDistricts = Array.from(new Set(allDistricts));

            // console.log(pincode, distinctDistricts);
            let districtMapping = JSON.parse(JSON.stringify(pincodeDistictMap));
            let newDistrictMapping = Object.assign(districtMapping, { [pincode]: distinctDistricts[0] })
            console.log(newDistrictMapping);
            setPinCodeDistrictMap(newDistrictMapping);
          }

        } else {
          console.log("no data found!")
        }
      }
    }
  }

  return (
    <div className="App">
      <Header />
      <UploadFile uploadHandler={onUploadHandler} />
      <EmployeeGrid employees={employees} mapping={pincodeDistictMap} />
      <ZoneGrid zones={zones} />
      {loader && <Loader />}
    </div>
  );
}

export default App;
