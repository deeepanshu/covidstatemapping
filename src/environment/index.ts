export default {
  urlStore: {
    zonalData: 'https://api.covid19india.org/zones.json',
    pinToDistrictMapping: 'https://api.postalpincode.in/pincode/'
  }
}