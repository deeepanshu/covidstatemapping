import React from 'react';
import { AgGridReact } from 'ag-grid-react';
import { zoneToClass } from '../../constants';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';


export default class ZoneGrid extends React.Component<any, any> {

  constructor(props: any) {
    super(props);

    this.state = {
      columnDefs: [
        { headerName: "State", field: "state" },
        { headerName: "District", field: "district" },
        {
          headerName: "Zone", field: "zone", cellRenderer: (row: any) => {
            let pEle = document.createElement('p');
            let pText = document.createTextNode(row.value);
            pEle.setAttribute("class", zoneToClass[row.value]);
            pEle.appendChild(pText);
            return pEle;
          }
        }
      ]
    }
  }

  render() {
    const { zones } = this.props;

    return (
      <div className="ag-theme-material pb-lg-3 pl-lg-5 pr-lg-5 pt-2 w-100">
        <AgGridReact
          animateRows={true}
          defaultColDef={{
            flex: 1,
            minWidth: 100,
            filter: true,
            sortable: true,
            resizable: true,
          }}
          pagination={true}
          paginationPageSize={10}
          domLayout='autoHeight'
          columnDefs={this.state.columnDefs}
          rowData={zones}
        />
      </div>
    )
  }

}

