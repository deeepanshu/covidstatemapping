import React from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';


export default class EmployeeGrid extends React.Component<any, any> {

  constructor(props: any) {
    super(props);

    this.state = {
      columnDefs: [
        { headerName: "Name", field: "name" },
        { headerName: "Code", field: "code" },
        { headerName: "Department", field: "department" },
        { headerName: "Address", field: "address" },
        { headerName: "Zone", field: "zone", cellRenderer: (row: any) => {
          console.log(this.props.mapping);
          let {data : {address}} = row;
          console.log(address);
          
        } }
      ]
    }
  }

  render() {
    const {mapping} = this.props;
    return (
      <div className="ag-theme-material pb-lg-3 pl-lg-5 pr-lg-5 pt-2 w-100">
        <AgGridReact
          animateRows={true}
          defaultColDef={{
            flex: 1,
            minWidth: 100,
            filter: true,
            sortable: true,
            resizable: true,
          }}
          pagination={true}
          paginationPageSize={10}
          domLayout='autoHeight'
          columnDefs={this.state.columnDefs}
          rowData={this.props.employees}
        />
      </div>
    )
  }

}

