import React from 'react';

export default () => (
  <div style={{
    position: "absolute",
    top: 0,
    left: 0,
    minWidth: '100%',
    minHeight: '100vh',
    backgroundColor: "rgba(1,1,1,0.5)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }}>
    <div className="loader"></div>
  </div>
)