import React from 'react';
import { Link } from 'react-router-dom';

import Logo from '../../assets/images/logo.png';
const Header = () => (
  <nav className="navbar navbar-expand-lg navbar-light">
    <Link className="nav-brand" to="/" >
      <img src={Logo} width="200" alt="" />
    </Link>
    
    {/* 
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button> 
    */}

    <div className="collapse navbar-collapse" id="navbarSupportedContent">


    </div>
  </nav>
)

export default Header;