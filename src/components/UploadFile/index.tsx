import React from 'react';

interface IUploadFile {
  uploadHandler: () => void
}

export default ({ uploadHandler }: IUploadFile) => (
  <div className="card mb-4 ml-lg-5 mr-lg-5 mt-lg-5 p-4 d-flex flex-row justify-content-between align-items-center">
    <div>
      <h2>Covid Hotspots</h2>
      <p>Please Press Upload to upload employee records.</p>
    </div>
    <div>
      <button className="btn btn-primary custom-button" onClick={uploadHandler}>Upload</button>
    </div>
  </div>
)