import { call, put } from 'redux-saga/effects';
import axios from 'axios';
import environment from '../../environment';
import { SET_ZONAL_DATA, SHOW_LOADER, HIDE_LOADER } from '../types';
import { IZone } from '../../interfaces';

export function* getZonalData() {
  try {
    yield put({ type: SHOW_LOADER })
    let response = yield call(axios, environment.urlStore.zonalData);
    if (response.status === 200) {
      let zones: IZone[] = Array.isArray(response.data.zones) ? response.data.zones : [];
      zones = zones.filter((ele) => ele.zone);

      console.log("Distinct District names", Array.from(new Set(zones.map(ele => ele.district))).length)

      console.log(zones.length);
      yield put({ type: SET_ZONAL_DATA, payload: zones })
    }
    console.log(response);
  } catch (error) {
    console.log(error);
  } finally {
    yield put({ type: HIDE_LOADER })
  }
}
