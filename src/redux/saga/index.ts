import { takeLatest } from 'redux-saga/effects';
import * as types from '../types';
import { getZonalData } from './data';

function* rootSaga() {
  yield takeLatest(types.GET_ZONAL_DATA, getZonalData);

}

export default rootSaga;