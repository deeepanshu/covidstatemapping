import { SET_ZONAL_DATA } from '../types';

const INITIAL_DATA = {
  zones: []
}

export default (state = INITIAL_DATA, action: any) => {
  switch (action.type) {
    case SET_ZONAL_DATA: 
      return {
        ...state,
        zones: action.payload
      }
    default: 
      return {
        ...state
      }
  }
}
