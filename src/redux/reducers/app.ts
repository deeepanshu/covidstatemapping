import { SHOW_LOADER, HIDE_LOADER } from '../types';

const INITIAL_DATA = {
  loader: false
}

export default (state = INITIAL_DATA, action: any) => {
  switch (action.type) {
    case SHOW_LOADER: 
      return {
        ...state,
        loader: true
      }
    case HIDE_LOADER: 
      return {
        ...state,
        loader: false
      }
    default: 
      return {
        ...state
      }
  }
}
