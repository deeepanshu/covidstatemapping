export const GET_ZONAL_DATA = "[ZONE] GET ZONE DATA";

export const SET_ZONAL_DATA = "[ZONE] SET ZONE DATA";

export const SHOW_LOADER = "[APP] SHOW LOADER";

export const HIDE_LOADER = "[APP] HIDE LOADER";
